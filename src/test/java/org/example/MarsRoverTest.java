package org.example;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

class MarsRoverTest {

    @Test
    void should_position_at_0_1_N_when_process_command_given_command_M() {
        var robot = new MarsRover();
        var position = robot.processCommands("M");
        assertEquals("0 1 N", position);
    }

    @Test
    void should_position_at_0_0_E_when_process_command_given_command_R() {
        var robot = new MarsRover();
        var position = robot.processCommands("R");
        assertEquals("0 0 E", position);
    }

    @Test
    void should_position_at_0_0_W_when_process_command_given_command_L() {
        var robot = new MarsRover();
        var position = robot.processCommands("L");
        assertEquals("0 0 W", position);
    }

    @Test
    void should_position_at_1_0_E_when_process_command_given_command_RM() {
        var robot = new MarsRover();
        var position = robot.processCommands("RM");
        assertEquals("1 0 E", position);
    }

    @Test
    void should_position_at_negative_1_0_W_when_process_command_given_command_LM() {
        var robot = new MarsRover();
        var position = robot.processCommands("LM");
        assertEquals("-1 0 W", position);
    }

    @Test
    void should_position_at_0_negative_1_S_when_process_command_given_command_RRM() {
        var robot = new MarsRover();
        var position = robot.processCommands("RRM");
        assertEquals("0 -1 S", position);
    }

    @Test
    void should_position_at_0_1_S_when_process_command_given_command_MRR() {
        var robot = new MarsRover();
        var position = robot.processCommands("MRR");
        assertEquals("0 1 S", position);
    }

    @Test
    void should_position_at_1_1_N_when_process_command_given_command_RMLM() {
        var robot = new MarsRover();
        var position = robot.processCommands("RMLM");
        assertEquals("1 1 N", position);
    }

    @Test
    void should_position_at_1_negative_1_S_when_process_command_given_command_RMRM() {
        var robot = new MarsRover();
        var position = robot.processCommands("RMRM");
        assertEquals("1 -1 S", position);
    }

    @Test
    void should_position_at_negative_1_1_N_when_process_command_given_command_LMRM() {
        var robot = new MarsRover();
        var position = robot.processCommands("LMRM");
        assertEquals("-1 1 N", position);
    }

    @Test
    void should_position_at_negative_1_negative_1_S_when_process_command_given_command_LMLM() {
        var robot = new MarsRover();
        var position = robot.processCommands("LMLM");
        assertEquals("-1 -1 S", position);
    }

}
