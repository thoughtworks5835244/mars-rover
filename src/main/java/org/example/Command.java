package org.example;

public enum Command {

    MOVE("M"),
    LEFT("L"),
    RIGHT("R");

    private final String value;

    Command(String value) {
        this.value = value;
    }

    public String getValue() {
        return value;
    }
}
