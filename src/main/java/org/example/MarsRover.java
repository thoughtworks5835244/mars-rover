package org.example;

import java.util.List;

public class MarsRover {

    private static final int MAX_DIRECTION = 4;
    private static final List<Direction> DIRECTIONS =
        List.of(Direction.NORTH, Direction.EAST, Direction.SOUTH, Direction.WEST);

    private int coordinateX = 0;
    private int coordinateY = 0;
    private Direction direction = Direction.NORTH;

    public String processCommands(String commands) {
        commands.chars().forEach(c -> this.updatePosition("" + (char) c));
        return String.format(
            "%d %d %s",
            this.coordinateX,
            this.coordinateY,
            this.direction.getValue()
        );
    }

    private void updatePosition(String command) {
        if (Command.MOVE.getValue().equals(command)) {
            move();
        } else if (Command.RIGHT.getValue().equals(command)) {
            turn(false);
        } else if (Command.LEFT.getValue().equals(command)) {
            turn(true);
        }
    }

    private void move() {
        switch (direction) {
            case NORTH:
                coordinateY += 1;
                break;
            case EAST:
                coordinateX += 1;
                break;
            case WEST:
                coordinateX -= 1;
                break;
            case SOUTH:
                coordinateY -= 1;
                break;
        }
    }

    private void turn(boolean isLeft) {
        var nextDirectionIndex =
            (DIRECTIONS.indexOf(direction) + 1) % MAX_DIRECTION;

        if (isLeft) {
            nextDirectionIndex =
                (DIRECTIONS.indexOf(direction) - 1 + MAX_DIRECTION) % MAX_DIRECTION;
        }

        direction = DIRECTIONS.get(nextDirectionIndex);
    }
}
